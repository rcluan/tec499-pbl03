#include <rs232.h>
#include <stdio.h>

#define BDRATE 115200
#define MODE "8N1"

int bdrate = BDRATE;
char *mode = MODE;
int cport_n = 0;
int isOpen = 0;

char port_names[22][12] = {"ttyS0","ttyS1","ttyS2","ttyS3","ttyS4",
                           "ttyS5","ttyS6","ttyS7","ttyS8","ttyS9",
                           "ttyS10","ttyS11","ttyS12","ttyS13","ttyS14","ttyS15",
                           "ttyUSB0","ttyUSB1","ttyUSB2","ttyUSB3","ttyUSB4","ttyUSB5"};

void binary_string(int val, char* bin) {
    int i;
    for (i = 31; i >= 0; i--) {
        if (val&(1<<i)) bin[31-i] = '1';
        else bin[31-i] = '0';
    }
    bin[32] = '\0';
}

void setup(){
    while (cport_n <= 21) {
        
        printf ("Trying port /dev/%s...\n",port_names[cport_n]);
        if (!RS232_OpenComport(cport_n, bdrate, mode)) {
            printf ("Port /dev/%s open!\n",port_names[cport_n]);
            isOpen = 1;
            break;
        }
        cport_n++;
    }
}

void read_serial() {
    unsigned char buf[4096];
    int n, i;

    int val = 0;
    int offset = 3;
    char bin_string[33];

    FILE* fp = fopen("out.txt","w+");

    printf ("\n\nWaiting Data...\n");
    while(1) {
        n = RS232_PollComport(cport_n, buf, 4095);

        if(n > 0) {
            i = 0;
            do {
                val |= (buf[i++]<<(offset*8));
                offset--;
                if (offset < 0) {
                    binary_string(val,bin_string);
                    printf ("%12d (%32s)\n",val,bin_string);
                    fprintf (fp,"%d\n",val);
                    val = 0;
                    offset = 3;
                }
            } while (i < n);
        }
    }
}

int main(){
    setup();
    if (isOpen)
        read_serial();
    else   
        printf ("\nNo port available.\n");
    
    return 0;
}