# Block 2 - LUI, SLL, ORI

.text
  .main:
    li   $t0, 65536
    li   $t1, 65537
    sw   $t1, 0($t0)
    
    sll  $t6, $t1, 2
    sw   $t6, 0($t0)
