module DataHandler(

  input Clock,
  input start,
  input [31:0] data_in,
  input tx_busy,

  output reg Tx_Wr_Enable = 0,
  output reg [13:0] mem_addr = 0,
  output reg [7:0] data_out = 0
);

initial begin
  Tx_Wr_Enable = 0;
  mem_addr = 14'h0;
end

  parameter IDLE = 2'b00;
  parameter COMM_UART = 2'b01;
  parameter WAIT = 2'b10;
  parameter ACCESS_MEM = 2'b11;


  reg [1:0] state = IDLE;
  reg [2:0] counter = 0;

  always @ ( posedge Clock ) begin

    case(state)

      IDLE: if(start) state <= COMM_UART;

      COMM_UART: begin

        if(~tx_busy) begin

          case(counter)

            3'b000: begin 
                data_out <= data_in[7:0];
                counter <= counter + 3'd1;
                Tx_Wr_Enable <= 1;
                state <= WAIT;
                end
            3'b001: begin 
                data_out <= data_in[15:8];
                counter <= counter + 3'd1;      
                Tx_Wr_Enable <= 1;          
                state <= WAIT;
                end
            3'b010: begin 
                data_out <= data_in[23:16];
                counter <= counter + 3'd1;  
                Tx_Wr_Enable <= 1;              
                state <= WAIT;
                end
            3'b011: begin 
                data_out <= data_in[31:24];
                counter <= counter + 3'd1; 
                Tx_Wr_Enable <= 1;               
                state <= WAIT;
                end
            default: begin 
                state <= ACCESS_MEM;
                counter <= 3'b0;
                Tx_Wr_Enable <= 0;
                end
          endcase
        end
        else state <= WAIT;
      end

      WAIT: begin 
          Tx_Wr_Enable <= 0;
          if(~tx_busy) state <= COMM_UART;
          end
      ACCESS_MEM: begin
          if (mem_addr == 14'd16383) state <= IDLE;
          else begin
              mem_addr <= mem_addr + 14'b1;
              state <= COMM_UART;
              end
          end
    endcase
  end
  /*
  always @(posedge Clock) begin

    if(~tx_busy && counter > 3) begin
      counter <= 0;
    end

    if(tx_busy) begin

      case (counter)

        0: data_out <= data_in[7:0];
        1: data_out <= data_in[8:15];
        2: data_out <= data_in[16:23];
        3: data_out <= data_in[31:24];
      endcase

      counter <= counter + 1'b1;
    end
  end
  */
endmodule
