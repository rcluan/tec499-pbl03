# Block 1 - ADD, ADDI, SUB, MUL, SLT

.text
  .main:
    li   $t0, 65536           # t0 = 65536 (controller's memory address)
    li   $s1, 1               # $s1 = 1
    li   $s2, 2
    li   $s3, 3
    li   $s4, 4

    sw   $t1, 0($t0)
    mul  $t1, $s1, $t0        # $t1 = $s1 * 65536
    sw   $t1, 0($t0)          # saves $t1 on first address of the controller's memory
    
    sw   $s1, 0($t0)
    addi $s1, $s1, 65537      # $s1 = $s1 + 65537
    sw   $s1, 0($t0)          # saves $s1 on first address of the controller's memory

    sw   $s2, 0($t0)
    sub  $s2, $s1, $t1        # $s2 = $s1 - $t1
    sw   $s2, 0($t0)          # saves $s2 on first address of the controller's memory

    sw   $s3, 0($t0)
    add  $s3, $s1, $t1	      # $s3 = $s1 + $t1
    sw   $s3, 0($t0)          # saves $s3 on first address of the controller's memory

    sw   $s4, 0($t0)
    slt  $s4, $s2, $zero      # $s4 recebe o valor 1 caso $s2 < zero
    sw   $s4, 0($t0)          # saves $s4 on first address of the controller's memory
