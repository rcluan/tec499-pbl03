# Block 4 - BEQ, BNE

.text
  main:
    li $t0, 65536
    li $t1, $zero
    li $t2, $zero
    
    beq $t1, $zero, add_routine     # goes to add_routine if $t1 == zero

  add_routine:
    addi $t2, $t2, 1                # $t2 += 1
    sw   $t1, 0($t0)
    
    bne	$t2, $zero, sub_routine     # goes to sub_routine if $t2 != zero
  
  sub_routine:
    sub  $t2, $t1, $t2              # $t2 = zero - 1
    sw   $t2, 0($t0)
