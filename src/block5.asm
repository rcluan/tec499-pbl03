# Block 5 - J, JAL, JR

.text
  main:
    li $t0, 65536
    li $s1, $zero
    
    sw $s1, 0($t0)
    jal jump_link
      
    addi $s1, $s1, 1
    sw $s1, 0($t0)    

    j exit

  jump_link:
    sw $ra, 0($t0)
    jr $ra

  exit:
    sw $ra, 0($t0)
    addi $s1, $s1, 1
    sw $s1, 0($t0)
